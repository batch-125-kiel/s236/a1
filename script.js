
//CREATE
	//insert into users
db.users.insertMany(

	[
		{"firstName": "Diane", "lastName": "Murphy", "email": "dmurphy@gmail.com", "isAdmin": false, "isActive":  true},
		{"firstName": "Mary", "lastName": "Patterson", "email": "mpatterson@gmail.com", "isAdmin": false, "isActive":  true},
		{"firstName": "Jeff", "lastName": "Firrelli", "email": "jfirrelli@gmail.com", "isAdmin": false, "isActive":  true},
		{"firstName": "Gerard", "lastName": "Bondur", "email": "gbondur@gmail.com", "isAdmin": false, "isActive":  true},
		{"firstName": "Pamela", "lastName": "Castillo", "email": "pcastillo@gmail.com", "isAdmin": true, "isActive":  false},
		{"firstName": "George", "lastName": "Vanauf", "email": "gvanauf@gmail.com", "isAdmin": true, "isActive":  true}

	]

);
	//insert into courses
db.courses.insertMany(
	[
		{"name": "Professional Development", "price": 10000}, 
		{"name": "Business Processing", "price": 13000 }
	]
);



//UPDATE courses, insert enrollees field
	//update professional development, add enrollees field with array value(user ObjectID)
db.courses.updateOne(
	{"name": "Professional Development"},
	
	{$set:
		{"enrollees": [ObjectId("61260cdffed43fe048ef380c"),ObjectId("61260cdffed43fe048ef380e")]}	
	}
);

	//update business processing, add enrollees field with array value(user ObjectID)
db.courses.updateOne(
	{"name": "Business Processing"},
	
	{$set:
		{"enrollees": [ObjectId("61260cdffed43fe048ef380f"),ObjectId("61260cdffed43fe048ef380d")]}
	}
);

//GET USERS WHO ARE NOT ADMIN

db.users.find({"isAdmin": false});

//display users
db.users.find();

//display courses
db.courses.find();




